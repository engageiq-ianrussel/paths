<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PathController extends WebTestCase
{
    /**
     * @test
     */
    public function showPaths()
    {
        $client = static::createClient();

        $client->request('GET', '/paths/all');

        $crawler = $client->request('GET', '/paths/all');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(
            0,
            $crawler->filter('h1')->count()
        );
    }
    /**
     * @test
     */
    public function showDashboard()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
