var requireAssetsHelper = require("encore-require-assets-helper");

requireAssetsHelper(
    "./assets/img/**/*.{jpg,jpeg,png,gif,svg,ico}",
    "./assets/js/assets.js",
    "",
    "../../"
);
