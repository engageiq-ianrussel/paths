<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PathController extends Controller
{
    /**
     * list of all paths
     */
    public function all()
    {
        return $this->render('paths/allpaths.html.twig', array('title' => 'All Paths'));
    }
    /**
     *  show our dashboard
     */
    public function index()
    {
        return $this->render('paths/index.html.twig', array('title' => 'Dashboard'));
    }
    /**
     *  show our form
     */
    public function showCreatePathForm()
    {
        return $this->render('paths/createPathForm.html.twig', array('title' => 'Create New Path'));
    }
}
