
require('../css/app.scss');
require('../css/style.css');
require('./assets.js');
// require('./master.js');
// loads the jquery package from node_modules
var $ = require('jquery');

// Loads popperjs
require('popper.js');
require('tooltip.js');
require('bootstrap/dist/js/bootstrap.js');
require('pace/pace.js');
require('./main.js');

// import the function from greet.js (the .js extension is optional)
// ./ (or ../) means to look for a local file
var greet = require('./greet');

// $(document).ready(function() {
//     $('body').prepend('<h1>'+greet('john')+'</h1>');
// });
