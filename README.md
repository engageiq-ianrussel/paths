# About this project
*Assign new task to Ian and Jeremie about the automation in creating a path with new design*

1. User can create a new path with new design.
1. The procedure should be automatic wherein user can just define the background color, text, header images, footer etc... and once done it will automatically create the new path in an instant and can be ready to use.
1. All google analytics, tag manager should also be automatically added.
1. Smartlook script can also be added automatically.
1. For point 4 and 5 above you don't have to worry on this since its already built in the NLR. every revenue tracker can just pull their corresponding google analytics, tag manager, smartlook script from the NLR as user lands on the landing page. This is ready to use.

As Sir Burt said:  :couple_with_heart:
> We're living the future so
> the present is our past.

# How To

* Install Php dependencies

1. git clone git@bitbucket.org:engageiq-ianrussel/paths.git
1. cd to project folder
1. composer install

* Install Javascript dependencies

1. npm install yarn //add -g flag if globally needed
1. yarn install

* Run your project, defaults to localhost:800*

1. php bin/console server:run

# During Development

1. yarn run dev // -> compile assets once
2. yarn run watch // -> recompile assets automatically when file changes
3. yarn run build // -> compile assets but also minify and optimize them

# Image Management

1. Add your images in assets/img
2. Run node assets.config.js // <- this is very important if you added new images
3. Run yarn watch

# Tests

1. ./vendor/bin/simple-phpunit

# Links

[Paths](http://automatedpath.paidforresearch.com/)
